/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recruitment.mcpayment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author rezaadrean
 */
public class No3 {

    private static String[] substraction(String s, Integer n) {

        List<String> list = new ArrayList<>();
        String[] splited = s.split("\\s+");
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() == n) {
                list.add(splited[i]);
            }
        }
        return list.toArray(new String[0]);

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);    
        System.out.print("Enter a string: ");
        String strs = sc.nextLine();
        System.out.print("Enter a length string: ");
        Integer a= sc.nextInt();
        String[] nu = new String[]{};
        nu = substraction(strs, a);
        System.out.println("The result of string elements in an array where string length is " + a.toString() + Arrays.toString(nu));

    }
}
