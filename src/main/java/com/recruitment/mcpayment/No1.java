/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.recruitment.mcpayment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author rezaadrean
 */
public class No1 {

    private static Integer[] substraction(Integer[] nums) {

        List<Integer> list = new ArrayList<>();
        int max = nums[0];
//        Arrays.sort(nums);
        for (int i = 1; i < nums.length; i++) { 
                if (nums[i] > max) {
                    max = nums[i];                                        
                }                    
        }
        list.add(max);
        return list.toArray(new Integer[0]);
    }

    public static void main(String[] args) {
        Integer[] nums;
        Scanner scanner = new Scanner(System.in);
        System.out.print("How many integers you want to enter: ");
        Integer n = 0;
        if (scanner.hasNextInt()) {
            n = scanner.nextInt();
            scanner.nextLine();
        }
        nums = new Integer[n];
        System.out.print("Enter the integers separated by a space: ");
        String[] strNums = null;
        if (scanner.hasNextLine()) {
            strNums = scanner.nextLine().split(" ");
        }
        if (strNums != null) {
            for (int i = 0; i < n; i++) {
                try {
                    nums[i] = Integer.parseInt(strNums[i]);
                } catch (Exception e) {
                    System.out.println("Invalid input");
                    break;
                }
            }
        }
        Integer[] nu = new Integer[]{};
        nu = substraction(nums);
        System.out.println("The result of subtracting elements in an array greater than 0 = " + Arrays.toString(nu));

    }
}
